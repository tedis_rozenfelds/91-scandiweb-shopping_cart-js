<?php

require_once "Product.php";

class Furniture extends Product
{      
    protected string $tableName = "furniture";
    protected string $attributeName = "dimensions";
}