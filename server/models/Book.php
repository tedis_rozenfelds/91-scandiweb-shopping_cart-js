<?php

require_once "Product.php";

class Book extends Product 
{      
    protected string $tableName = "book";
    protected string $attributeName = "weight";
}