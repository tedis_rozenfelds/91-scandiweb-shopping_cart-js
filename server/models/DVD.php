<?php

require_once "Product.php";

class Dvd extends Product
{      
    protected string $tableName = "dvd";
    protected string $attributeName = "size";
}
